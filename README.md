## kdtemporal

`kdtemporal` is a package that allows the estimation of the diffuse attenuation coefficient on the time series of Sentinel-2/MSI images. This package was created during the development of the `Mapping of diffuse attenuation coefficient in optically complex waters of amazon floodplain lakes` article. For more information on the method used, please refer to the cited article.

**DOI**: [10.1016/j.isprsjprs.2020.10.009](https://www.sciencedirect.com/science/article/abs/pii/S0924271620302811)

### Instalation

To install the package you need to install the [remotes package](https://cran.r-project.org/web/packages/remotes/remotes.pdf) and install kdtemporal. In a R terminal use:

```r
install.packages("remotes")
```

After that,

```r
remotes::install_gitlab("labisa/kd/kd-qaa-temporal")
```

### Usage

The use of the package is divided into four main parts, these being:

**Reference table preparation**


The first step is to create the reference table. This table contains all the information from the data files and the bands to be used. The table to be created must contain the following columns:

- Columns `B02`, `B03`, `B04`, `B05`, `B06` and `B11`. These columns contain the reference to the files for each of the listed Sentinel-2/MSI bands used in the processing;
- Column `cloud_mask`: FMask-4 file path generated for each scene used in the processing;
- Column `kd_result_path`: Path (with filename included) where result is saved

The table below shows an example of a structure:

| B02        | B03        | B04        | B05        | B06        | B11        | cloud_mask    | kd_result_path |
|------------|------------|------------|------------|------------|------------|---------------|----------------|
| band02.tif | band03.tif | band04.tif | band05.tif | band06.tif | band11.tif | cloudmask.tif | result.tif     |


**Region Of Interest definition**

In this second step, it is necessary to define the **R**egion **O**f **I**nterest (ROI) that will be used. To do this, create a `ESRI Shapefile` with the ROI. The shapefile must be saved in the data projection to avoid transformations and distortions on-the-fly.

**Absorption table**


The table of absorptions is essential for the correct functioning of the data. In this step, after defining which information will be used, the data must be organized in the structure shown in the table below.

| Wave | Abs                | bb                   |
|------|--------------------|----------------------|
| 490  | 0.0212043446720684 | 0.000249350553251604 |
| 560  | 0.0630589671477453 | 0.000144046341914815 |
| 665  | 0.413953325957096  | 7.06992713197061e-05 |
| 705  | 0.703857578762181  | 5.56407682417169e-05 |
| 740  | 2.61821730665308   | 4.52225866876588e-05 |
| 783  | 2.24485592224579   | 3.60112148995556e-05 |
| 842  | 3.23211752008225   | 2.84216230719257e-05 |
| 865  | 4.55087740413135   | 2.39384797221919e-05 |

**Code execution**

Now, all the elements generated in the previous steps can be unified to create the `diffuse attenuation coefficient` estimates. The code below shows an execution example. Then the result of the execution is shown.

```r
library(readr)
library(kdtemporal)

#
# Load reference table
#
images <- readr::read_csv("kd_estimation_files.csv")

#
# ROI
#
shp_path <- "my-roi.shp"

#
# Absorption table
#
abs_table <- "w_and_bb_s2a.csv"

# run!
diffuse_attenuation_map(images, shp_path, abs_table, 6)
```

**Result**

<div align="center">
    <img src=".gitlab/result-example.png">
</div>
